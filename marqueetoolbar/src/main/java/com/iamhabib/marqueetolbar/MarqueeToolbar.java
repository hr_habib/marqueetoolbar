package com.iamhabib.marqueetolbar;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import java.lang.reflect.Field;

/**
 * Created by HABIB on 11/4/2016.
 */

public class MarqueeToolbar extends Toolbar {

    TextView title, subTitle;
    private int repeatCount = -1;
    public static final int INFINITY = -1;

    boolean isMarqueeTitle = false, isMarqueeSubTitle = false;

    public MarqueeToolbar(Context context) {
        super(context);
    }

    public MarqueeToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MarqueeToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setRepeatLimit(int repeatLimit) {
        this.repeatCount = repeatLimit;
    }

    @Override
    public void setTitle(CharSequence title) {
        if (!isMarqueeTitle) {
            isMarqueeTitle = reflectTitle();
        }
        super.setTitle(title);
    }

    @Override
    public void setTitle(int resId) {
        if (!isMarqueeTitle) {
            isMarqueeTitle = reflectTitle();
        }
        super.setTitle(resId);
    }

    private boolean reflectTitle() {
        try {
            Field field = Toolbar.class.getDeclaredField("mTitleTextView");
            field.setAccessible(true);
            title = (TextView) field.get(this);
            title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            title.setMarqueeRepeatLimit(repeatCount);
            title.setSelected(true);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override

    public void setSubtitle(CharSequence subTitle) {
        if (!isMarqueeSubTitle) {
            isMarqueeSubTitle = reflectSubTitle();
        }
        super.setSubtitle(subTitle);
    }


    @Override
    public void setSubtitle(int resId) {
        if (!isMarqueeSubTitle) {
            isMarqueeSubTitle = reflectSubTitle();
        }
        super.setSubtitle(resId);
    }

    private boolean reflectSubTitle() {
        try {
            Field field = Toolbar.class.getDeclaredField("mSubtitleTextView");
            field.setAccessible(true);
            subTitle = (TextView) field.get(this);
            subTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            subTitle.setMarqueeRepeatLimit(repeatCount);
            subTitle.setSelected(true);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
