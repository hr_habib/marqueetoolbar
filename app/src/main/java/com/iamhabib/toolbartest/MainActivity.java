package com.iamhabib.toolbartest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.iamhabib.marqueetolbar.MarqueeToolbar;

public class MainActivity extends AppCompatActivity {

    MarqueeToolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar=(MarqueeToolbar)findViewById(R.id.toolbar);
        toolbar.setRepeatLimit(MarqueeToolbar.INFINITY); // default infinity you can set integer number exp: 1, 2, 3, ...
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("This is the MarqueeToolbar title which is going right to left");
        getSupportActionBar().setSubtitle("This is the MarqueeToolbar subtitle");
    }
}
